all:
	gcc *.c -Og -pthread -Wall -o ./bin/rollercoaster

.PHONY: clean
clean: 
	rm -f ./obj/*.o ./bin/* 

