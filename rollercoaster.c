// README -----------------------------------------------------------------------
/*
AUTHOR: 
	estefano-ppt

DESCRIPTION:
	This program seeks to implement a more extended solution to
	Problem 5 of the Operating System's term 1, year 2018, exam. 
	By "extended" I mean that this solution makes sure that
	all the passenger threads receive attention, instead of leaving it
	to luck (in the exam's solution, it is not guaranteed that all
	the passengers make it into the ride). I've used an additional
	semaphore for this matter. I've also make the 'cart' an actual
	data-structure, and the passengers actually have to 'move in'
	and 'move out' of the 'cart'.

	This solution uses:
	1. POSIX Semaphores
	2. POSIX Threads (pthreads)
	3. POSIX Mutexes

	I've stayed as ANSI C-compliant as possible.

	Yes, there's a lot of code, but that's mostly because I tried to
	create a well-designed solution to the problem, and because
	the code is HEAVILY commented. My advice? Go straight to the
	main() function, read it quickly (it is well commented too),
	and then head for the passenger_ride() and cart_cycle() functions,
	which are the functions that handle the passenger and cart threads,
	respectively.

	Also, at first, the best thing that you can do is just run it. Compile 
	it with:

	$> make clean; make

	Then just run:

	$> ./bin/rollercoaster. 

PUZZLE:
	Here's a question that, if you can answer it, it's because you've
	really understood pretty well everything in this code:

	Why does a deadlock happens if N or C aren't a power of 2? What
	modifications would you make to avoid this deadlock?	

NOTE: 
	The reason I write in english is because portable C programs cannot
	contain Unicode characters, which doesn't let me put accent characters
	to spanish vowels. Call me whatever you want. I need my tildes man.

	Oh, and by the way, all the passengers here are girls because girls
	are cute.

LICENSE:
	This code is subject to Estefano Palacios's do-whatever-you-fucking-want-with-it
	license. If you wish to pay off the favor, introduce me to any girl friends you
        might have. Hehe. Have a good one!
*/


// INCLUDES ---------------------------------------------------------------------

#include <stdio.h>
#include <string.h>	
#include <stdlib.h>
#include <unistd.h>

#include <pthread.h> 
#include <semaphore.h>
 
// PRINCIPAL CONSTANTS ----------------------------------------------------------
/*
 * Constants N and C as defined in the exam's problem statement. N is the
 * [N]umber of waiting passengers. C is the [C]art's size. RIDE_DURATION
 * is the amount of time that the ride lasts. Note that they are power
 * of two on purpose (see PUZZLE section on README above). TIME_TO_WAIT_BEFORE_REENTRY
 * is the time the passenger and cart threads wait before restarting a loop,
 * this allows you to understand better what's going on just by browsing
 * the output.
 */

#define N 128
#define C 8

const size_t RIDE_DURATION = 3;
const size_t TIME_TO_WAIT_BEFORE_REENTRY = 2;

// PRINCIPAL TDAs  --------------------------------------------------------------
/*
 * GENERAL DESCRIPTION:
 * These define a type for passenger id's and associated
 * data structures to manage the passengers and the cart.
 * Passenger IDs of 0 mean 'no passenger here'. Thus, when
 * an element in cart[] has a value 0, it means the seat
 * is available. We're going to number the passengers
 * from MIN_ID to MIN_ID + N.
 */ 

// PASSENGER VARIABLES ---------------------------
// Just a typedef to abstract the underlying
// data-type for passenger IDs.
typedef unsigned int passenger_id_t;		

// Baseline id for passengers.
const passenger_id_t MIN_ID = 128;		

// Holds the passenger IDs. 
passenger_id_t passengers[N];			

// CART VARIABLES --------------------------------
// We need to save the cart's state so that we can
// coordinate actions with the passenger threads.
enum CartState {
	WAITING,
	RUNNING,
	STOPPED,
	UNLOADING
} cart_state;

// Cart data structure. Holds passengers for the ride.
passenger_id_t cart[C];	

/*
  This variable here is used by assign_seat() and passenger_sit() to
  keep track of the next available seat, and reset() on cart_cycle when
  the ride's over.
 */
size_t next_available_seat = 0;

// THREADS  ---------------------------------------------------------------------
pthread_t passenger_thread[N];
pthread_t cart_thread;

// SYNCHRONIZATION DEVICES ------------------------------------------------------
/*
 * This region defines all the synchronization devises--mutexes and
 * semaphores--that we will use.
 */

// Used to synchronize passenger threads so that all passengers get a 
// chance to enjoy the ride.
sem_t back_of_queue_sem;			

// Used to synchronize passenger threads so that only C passengers get
// sitted into the cart[].
sem_t passenger_entry_sem;			

// We use this mutex array to make the selected passengers wait until
// the cart is ready to board.
pthread_mutex_t cart_seat_ready_mtx[C];

// We use this mutex array to inform the cart when a passenger is seated.
// We do this because the problem statement says the cart can't move
// unless all passengers have been confirmed to have sitted.
pthread_mutex_t passenger_seated_mtx[C];

// We use this mutex array to make the passengers wait until the cart
// has announced that it is ready to unload. We do this because the
// problem statement says that the passengers can't leave the cart
// unless the cart has announced that it has stopped moving.
pthread_mutex_t remain_seated_mtx[C];

// The cart can't take any new passengers untill all the current
// passengers have freed the seat.
pthread_mutex_t seat_free_mtx[C];

// Use to synchronize assing_seat(), sit_passenger(), and unsit_passenger().
pthread_mutex_t cart_load_mtx;			


// FUNCTION FORWARD-DECLARATIONS  -----------------------------------------------

/*
 * A simple exit-on-error utility function.
 */
void err_exit(const char* msg){
	// Prints output on stderr (file-descriptor 2).
	perror(msg);		
	exit(EXIT_FAILURE);
}

/*
 * We want to flush stdout buffers on exit.
 */
void flush_stdio_onexit(){
	fflush(stdout);
}

/*
 * Forward declarations of the functions where the passenger and cart
 * threads will run. See the comments for each function below for more information.
 */
void* passenger_ride(void* arg);
void* cart_cycle(void* arg);

/*
 * These functions embody the operations that the cart does as
 * it moves through its steps. They are called in the cart_cycle() function.
 */
void load_cart();
void rollercoaster_go();
void unload_cart();

/*
 * This is the interface to work with the 'cart'. This modifies the 
 * cart[] array.
 */
size_t assign_seat();
void sit_passenger(size_t seat, passenger_id_t id);
void unsit_passenger(size_t seat);
void clear_seats();



// ------------------------------------------------------------------------------
// THE MAIN FUNCTION
// ------------------------------------------------------------------------------
/*
 * Our main function. It creates the N passenger threads running passenger_ride()
 * and cart_cyle(). It also initializes all the relevant data structures and
 * synchronization devices.
 */
int main(int argc, const char* argv[]){

	// We set an exit handler so that stdio buffers are flushed on exit.
	// It is not too important that you understand this line.
	atexit(flush_stdio_onexit);

	// To get prompt feedback, we set the stdio buffers to null. Otherwise
	// printf() output is buffered, which means that if we send SIGINT to
	// the process, we might not have seen all the actual work done
	// by the application. It is not too important that you understand this line.
	setvbuf(stdout, NULL, _IONBF, 0);

	// Initialize the passenger IDs. Every passenger thread is going to
	// use each passenger ID so that we can make more sense of the ouput.
	for (size_t i = 0; i < N; i++){
		passengers[i] = MIN_ID + i;
	}

	// Setting the initial configuration for the cart to zero,
	// which means that no seats are taken.
	memset(cart, 0, C * sizeof(passenger_id_t));

	// We initialize the cart with its initial state.
	cart_state = WAITING;
	
	// Initialize the synchronization devices. 
	
	// These two semaphores are best understood by looking at their context
	// in the passenger_ride() function. Just take note that one semaphore is
	// initialized with a value of N and the other with value of C, this is
	// important..
	sem_init(&back_of_queue_sem, 0, N);  
	sem_init(&passenger_entry_sem, 0, C);

	for (size_t i = 0; i < C; i++){
		// These mutexes are used to make passengers wait for 
		// the cart to be 'ready' before trying to take a seat.
		pthread_mutex_init(&cart_seat_ready_mtx[i], NULL);
		pthread_mutex_lock(&cart_seat_ready_mtx[i]);

		// These mutexes are used to inform the cart
		// that a particular passengers have taken a seat.
		// Once all seated passengers inform the cart 
		// that they have sitted, the cart can commence
		// its ride.
		pthread_mutex_init(&passenger_seated_mtx[i], NULL);
		pthread_mutex_lock(&passenger_seated_mtx[i]);

		// These mutexes force passengers to remain seated
		// until the cart has stopped moving. The cart
		// itself unlocks these mutexes to allow passengers
		// to move.
		pthread_mutex_init(&remain_seated_mtx[i], NULL);
		pthread_mutex_lock(&remain_seated_mtx[i]);

		// These mutexes represent passengers informing
		// the cart that they have left the seat. Once
		// all passengers have left their prior seats empty,
		// the cart can move to take in new passengers.
		pthread_mutex_init(&seat_free_mtx[i], NULL);
		pthread_mutex_lock(&seat_free_mtx[i]);
	}	

	// This is the mutex that we will use inside the cart-modifying
	// functions so that multiple threads don't overwrite over the
	// cart's information. This is better understand in the
	// context that it is used, which is in the assign_seat(),
	// sit_passenger(), unsit_passenger(), and clear_seats()
	// functions.
	pthread_mutex_init(&cart_load_mtx, NULL);

	// Just a friendly message to let us know we've started.
	printf("ROLLER COASTER SYSTEM INITIALIZED.\n");
	printf("CREATING PASSENGER AND CART THREADS.\n");

	// We create the cart thread. Since there's only one cart
	// servicing the passengers, we create one thread.
	pthread_create(&cart_thread, NULL, cart_cycle, NULL);

	// We create the passenger threads. Since there are N 
	// total passengers, we create N threads. Notice that
	// the argument is the passenger's IDs. 
	for (size_t i = 0; i < N; i++){
		pthread_create(&passenger_thread[i], NULL, passenger_ride, &passengers[i]);
	}

	// Waiting on threads so application doesn't exit, but we never really
	// reach beyond the first join.
	for (size_t i = 0; i < N; i++){
		pthread_join(passenger_thread[i], NULL);
	}

	pthread_join(cart_thread, NULL);

	// Like I said, we're never reaching this part. Make sure you 
	// understand why.
	exit(EXIT_SUCCESS);
}


// ------------------------------------------------------------------------------
// THE PASSENGER RIDE THREADING FUNCTION.
// ------------------------------------------------------------------------------
/*
 * This is the passenger_ride() function definition, which
 * abstracts the concept of a passenger doing the followiong
 * actions:
 * 1. Waiting in line.
 * 2. Getting onboard the cart[].
 * 3. Enjoying the ride.
 * 4. Getting out of the cart[].
 * 5. Getting back in line.
 */
void* passenger_ride(void* arg){
	passenger_id_t* pass_id = (passenger_id_t*) arg;

	/*
	 * We assume that the passengers are getting off the rollercoaster
	 * and immediately joining back in the queue, that's why we
	 * loop forever.
	 */
	while (1){
		// This semaphore here is meant to let the
		// re-joining passengers wait until the other passengers
		// have made a ride. Once all passengers have made the
		// ride, the thread that runs cart_cycle() will bring
		// this semaphore back to a value of N, and then all
		// passengers will get a chance to do the ride again.
		printf("PASSENGER %u: Waiting outside queue.\n", *pass_id);
		sem_wait(&back_of_queue_sem);

		// We let only C passengers move forward to take a seat.
		printf("PASSENGER %u: Has entered the queue. Waiting in line.\n", *pass_id);
		sem_wait(&passenger_entry_sem);

		// We need to 'reserve' a seat in the cart.
		size_t seat = assign_seat();

		// We wait here until cart is "ready" to let passengers in.
		printf("PASSENGER %u: Admitted into cart's doorstep. Seat %lu assigned.\n", *pass_id, seat);
		pthread_mutex_lock(&cart_seat_ready_mtx[seat]);
		
		// We sit a passenger, and obtain her seat so that we can 
		// unseat her when the ride is over. This call also 
		// informs cart_cycle() that the passenger is seated.
		sit_passenger(seat, *pass_id);

		// We use a spinlock to wait for other passengers
		// because a semaphore would be too costly and would
		// preempt the threads too many times for my taste.
		printf("PASSENGER %u: Waiting for cart to start.\n", *pass_id);
		while (cart_state == WAITING);

		// Here the passenger is just 'having fun with it'.
		// This is meant to simulate the passenger enjoying
		// the rollercoaster ride.
		while (cart_state == RUNNING){
			printf("PASSENGER %u (in seat %lu): \"Jieeeehaaaaaaa!\"\n", *pass_id, seat);
		}

		// The passengers shouldn't exit the cart until the cart stops moving, so the
		// cart_cycle() function needs to unlock the semaphores remain_seated_mtx[]
		// so that each passenger can get off her seat.
		printf("PASSENGER %u: \"Wham! What a ride! I want to go again!\"\n", *pass_id);
		printf("PASSENGER %u: Waiting for cart to stop so she can get off.\n", *pass_id);
		pthread_mutex_lock(&remain_seated_mtx[seat]);

		printf("PASSENGER %u: Cart has come to a full, stop. Getting off now.\n", *pass_id);

		// We make the passengers "get off" the cart.
		unsit_passenger(seat);
		
		// Wait some time so we can browse the output.
		sleep(TIME_TO_WAIT_BEFORE_REENTRY);
	}


	return NULL;
}

// ------------------------------------------------------------------------------
// THE CART CYCLE THREADING FUNCTION
// ------------------------------------------------------------------------------
/*
 * This is the cart_cycle() definition, which abstracts
 * the concept of the 'rollercoaster ride'. It performs
 * the following steps:
 * 1. Wait until passengers load.
 * 2. Take the ride.
 * 3. Wait until passengers disembark.
 */
void* cart_cycle(void* arg){

	size_t passengers_serviced = 0;
	while (1){
		// Informs passengers that they can begin loading the cart.
		// This is the 'cargar()' routine defined in the problem
		// statement.
		load_cart();	

		// We must wait for all passengers to be sitted
		// in order to start the roller coaster ride.
		printf("CART: Waiting for all passengers to be seated.\n");
		for (size_t i = 0; i < C; i++){
			printf("CART: Waiting for passenger to sit on seat %lu.\n", i);
			pthread_mutex_lock(&passenger_seated_mtx[i]);
			printf("CART: Passenger %u took a seat on seat %lu.\n", cart[i], i);
		}

		// Once all passengers are in, we run!
		// This is the 'moverse()' routine defined
		// in the problem statement.
		rollercoaster_go();

		// We unload the cart, which means informing
		// the passengers that they can finally get off.
		// This is the 'descargar()' routine in the 
		// problem statement.
		unload_cart();

		// We wait until all the seats have been freed.
		for (size_t i = 0; i < C; i++){
			printf("CART: Waiting for seat %lu to be freed.\n", i);
			pthread_mutex_lock(&seat_free_mtx[i]);
		}

		// We move the cart to waiting state again.
		cart_state = WAITING;

		// We need to keep a tally of the passengers so far serviced
		// to keep recently-serviced passengers from enjoying a 
		// ride (and let other passengers in).
		passengers_serviced += C;

		// We need to reset all the synchronization devices.
		//
		// If we have serviced all the passengers, then let
		// all of them into the queue again
		if (passengers_serviced >= N){
			for (size_t i = 0; i < N; i++){
				sem_post(&back_of_queue_sem);
			}
			passengers_serviced = 0;
		}
		
		// We make all seats 'available'.
		clear_seats();

		// We then let only C passengers into the small queue to
		// get assigned a seat.
		for (size_t i = 0; i < C; i++){
			sem_post(&passenger_entry_sem);
		}

		// Wait some time so we can browse the output.
		sleep(TIME_TO_WAIT_BEFORE_REENTRY);
	}
	
}


// ------------------------------------------------------------------------------
// CART STATE CHANGE 
// ------------------------------------------------------------------------------
/*
 * This function unlocks all the passenger mutexes and lets them
 * sit in the cart.
 */
void load_cart(){
	printf("CART: Cart has parked and is ready to take passengers.\n");

	for (size_t i = 0; i < C; i++){
		pthread_mutex_unlock(&cart_seat_ready_mtx[i]);
	}
}

/*
 * This is the function that represents the cart
 * rollercoaster ride.
 */
void rollercoaster_go(){

	printf("CART: Taking off! Enjoy the ride!\n");
	cart_state = RUNNING;

	// We let the thread sleep to emulate the "ride's happening"
	// concept. 
	sleep(RIDE_DURATION);

	printf("CART: \"We've reached the end of the ride, hoped you had a good one!\"\n");
	cart_state = STOPPED;

	return;
}

/*
 * This is function informs passengers that they can get off.
 */
void unload_cart(){

	printf("CART: The cart has come to a full stop. All dismissed!\n");
	cart_state = UNLOADING;

	// We inform all passengers that they can get off.
	for (size_t i = 0; i < C; i++){
		pthread_mutex_unlock(&remain_seated_mtx[i]);
	}

}

// ------------------------------------------------------------------------------
// CART PASSENGER-SITTING INTERFACE
// ------------------------------------------------------------------------------
/*
 * Passengers need to be assigned a seat.
 */
size_t assign_seat(){
	if (next_available_seat == C) 
		err_exit("sit_passenger(), no more seats available.\n");

	pthread_mutex_lock(&cart_load_mtx);
		size_t seat = next_available_seat++;
	pthread_mutex_unlock(&cart_load_mtx);

	return seat;
}

/*
 * This seats a passenger on the next available seat.
 * If there are no seats, this returns the value of
 * C to signify error. This is the 'abordo()` function
 * in the problem statement.
 */
void sit_passenger(size_t seat, passenger_id_t id){
	if (seat >= C) 
		err_exit("sit_passenger(), no more seats available.\n");

	pthread_mutex_lock(&cart_load_mtx);
	
		// We 'sit' the passenger by assigning his ID to the cart[].
		printf("PASSENGER %u: Taking seat %lu on cart.\n", id, seat);
		cart[seat] = id;

	pthread_mutex_unlock(&cart_load_mtx);

	// The cart_cycle() is waiting for all passengers to be seated
	// before it can start the ride.
	pthread_mutex_unlock(&passenger_seated_mtx[seat]);

	return;
}

/*
 * This function takes a seat, and 'unsits' the passenger.
 */
void unsit_passenger(size_t seat){
	if (seat > C) 
		err_exit("unsit_passenger(), requested seat is bigger than cart[] size.");

	pthread_mutex_lock(&cart_load_mtx);

		// Remember that '0' means no passenger seated here.
		printf("PASSENGER %u: leaves seat %lu.\n", cart[seat], seat);
		cart[seat] = 0;

	pthread_mutex_unlock(&cart_load_mtx);

	// We let the cart know we've freed the seat.
	pthread_mutex_unlock(&seat_free_mtx[seat]);
	
	return;
}

/*
 * The cart needs to reset the available seats.
 */
void clear_seats(){
	if (next_available_seat != C) 
		err_exit("assign_seat(), for some reason not all seats where occupied.\n");

	pthread_mutex_lock(&cart_load_mtx);
		next_available_seat = 0;
	pthread_mutex_unlock(&cart_load_mtx);

	return;
}
